package com.example.securityauth0.dto.response;

import lombok.*;

@Value
public class JwtResponse {
    String token;

}
