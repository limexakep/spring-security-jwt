package com.example.securityauth0.dto.request;

import lombok.*;

@Value
public class RegistrationRequestDto {
    String username;

    String password;
}
