package com.example.securityauth0.dto.request;

import lombok.*;

@Value
public class LoginRequestDto {
    String username;
    String password;
}
