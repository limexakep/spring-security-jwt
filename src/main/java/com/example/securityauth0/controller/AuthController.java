package com.example.securityauth0.controller;

import java.util.*;
import java.util.stream.*;

import com.example.securityauth0.dto.request.*;
import com.example.securityauth0.dto.response.*;
import com.example.securityauth0.entity.*;
import com.example.securityauth0.repository.*;
import com.example.securityauth0.security.*;
import com.example.securityauth0.service.*;
import lombok.*;
import org.springframework.http.*;
import org.springframework.security.authentication.*;
import org.springframework.security.core.*;
import org.springframework.security.core.context.*;
import org.springframework.security.crypto.password.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthenticationManager authenticationManager;

    private final JwtUtils jwtUtils;

    private final UserService userService;

    private final PasswordEncoder passwordEncoder;

    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUser(@RequestBody LoginRequestDto loginRequestDto) {
        Authentication authentication = authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                loginRequestDto.getUsername(),
                loginRequestDto.getPassword()
            )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@RequestBody RegistrationRequestDto registrationRequestDto) {
        if (userService.existsByUsername(registrationRequestDto.getUsername())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username already exist!");
        }

        User user = new User(
            registrationRequestDto.getUsername(),
            passwordEncoder.encode(registrationRequestDto.getPassword())
        );

        Set<Role> roles = new HashSet<>();
        roles.add(new Role("ROLE_USER"));

        user.setRoles(roles);

        userService.createUser(user);

        return ResponseEntity.ok("success");
    }
}
