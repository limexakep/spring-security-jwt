package com.example.securityauth0.controller;

import org.springframework.security.access.prepost.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/test")
public class TestController {
    @GetMapping("/all")
    public String sayHelloAll() {
        return "Public content";
    }

    @GetMapping("/user")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public String sayHelloUser() {
        return "User content";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String sayHelloAdmin() {
        return "Admin content";
    }
}
