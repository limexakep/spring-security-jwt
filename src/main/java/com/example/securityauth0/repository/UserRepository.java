package com.example.securityauth0.repository;

import com.example.securityauth0.entity.*;
import org.springframework.data.repository.*;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(String username);

    boolean existsByUsername(String username);
}
