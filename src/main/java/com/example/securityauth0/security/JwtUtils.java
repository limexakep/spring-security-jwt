package com.example.securityauth0.security;

import java.util.*;

import com.auth0.jwt.*;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.*;
import com.auth0.jwt.exceptions.*;
import com.auth0.jwt.interfaces.*;
import com.example.securityauth0.entity.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.*;
import org.springframework.stereotype.*;

@Component
public class JwtUtils {
    @Value("${plugin.jwt.secret}")
    private String jwtSecret;

    @Value("${plugin.jwt.expiration}")
    private String jwtExpirationTime;

    @Value("${plugin.jwt.issuer}")
    private String jwtIssuer;

    public String generateJwtToken(Authentication authentication) {
        User userPrincipal = (User) authentication.getPrincipal();

        Algorithm algorithm = Algorithm.HMAC256(jwtSecret);

        return JWT.create()
            .withIssuer(jwtIssuer)
            .withSubject(userPrincipal.getUsername())
            .withClaim("userId", userPrincipal.getId())
            .withIssuedAt(new Date())
            .withExpiresAt(new Date(System.currentTimeMillis() + Long.parseLong(jwtExpirationTime)))
            .withJWTId(UUID.randomUUID().toString())
            .withNotBefore(new Date(System.currentTimeMillis() + 1000L))
            .sign(algorithm);
    }

    public boolean validateJwtToken(String jwtToken) {
        Algorithm algorithm = Algorithm.HMAC256(jwtSecret);

        JWTVerifier verifier = JWT.require(algorithm)
            .withIssuer(jwtIssuer)
            .build();

        try {
            DecodedJWT decodedJWT = verifier.verify(jwtToken);
            return true;
        } catch (JWTVerificationException e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    public String getUserNameFromJwtToken(String jwtToken) {
        Algorithm algorithm = Algorithm.HMAC256(jwtSecret);

        JWTVerifier verifier = JWT.require(algorithm)
            .withIssuer(jwtIssuer)
            .build();

        try {
            DecodedJWT decodedJWT = verifier.verify(jwtToken);

            return decodedJWT.getSubject();
        } catch (JWTVerificationException e) {
            System.out.println(e.getMessage());
        }

        return null;
    }
}
