CREATE TABLE "user"
(
    id       SERIAL8,
    username VARCHAR(50),
    password VARCHAR(255),
    enabled  BOOLEAN,
    CONSTRAINT pk_user PRIMARY KEY (id)
);
