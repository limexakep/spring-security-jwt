CREATE TABLE role
(
    id   SERIAL8,
    name VARCHAR(50),
    CONSTRAINT pk_role PRIMARY KEY (id)
);
